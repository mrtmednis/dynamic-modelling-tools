# Dynamic modelling tools
![Plot](https://github.com/MartinsMednis/dynamic-modelling-tools/blob/master/plot_22-A1.png)

The modelling tools are completely scriptable. The experimental data and paralelization can be configured in serial_run_model.sh.
