#!/usr/bin/python

import sys
# print sys.argv

if len(sys.argv)>2:
	# print sys.argv[1]
	import gi
	gi.require_version('Notify', '0.7')
	
	from gi.repository import Notify
	Notify.init ("{0}".format(sys.argv[1]))
	Hello=Notify.Notification.new ("{0}".format(sys.argv[1]),
	                               "{0}".format(sys.argv[2]),
	                               "dialog-information")
	Hello.show ()
