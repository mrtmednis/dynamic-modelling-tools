#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
sys.path.append("/home/me/mypy")
import os.path
import numpy as np
# from copy import deepcopy
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series

from modelling_utilities import *

# from ethanol_models import zafarLP

from model_configuration import model_options


# Process command-line arguments and determine experiment numbers
if "--model" in sys.argv:
	model_name = sys.argv[sys.argv.index('--model')+1] # one argument passed after --parameters
else:
	raise Exception('Model not specified.')

if "--exptitle" in sys.argv:
	exptitle = sys.argv[sys.argv.index('--exptitle')+1] # one argument passed after --exptitle


if "--path" in sys.argv:
	path = sys.argv[sys.argv.index('--path')+1] # one argument passed after --path
else:
	path="./"

if "--paramrow" in sys.argv:
	paramrow = sys.argv[sys.argv.index('--paramrow')+1] # one argument passed after --parameters
	paramrow = int(paramrow)
else:
	paramrow = -1

if "--experiments" in sys.argv:
	expnoList = sys.argv[sys.argv.index('--experiments')+1:] # everything passed after --experiments

# Should we estimate new parameters or simply run simulation
estimate_parameters = (True if "-estimate" in sys.argv else False)

# Should we save simulation results in parameter table
savesim = (True if "-savesim" in sys.argv else False)



## load measurements data
ethanol_all = pd.read_pickle('ethanol_all2.pickle')
all_data = load_experimental_data(ethanol_all, expnoList)


# Configure the model
path,model,bounds = model_options(model_name,exptitle)
# path = "rezultati_zafarLP/"
# model = zafarLP("zafarLP", exptitle=exptitle, path=path, save=True)
# #          miu_max, 	Yxs, 	   	mS, 	Ks,          Kis,      Kip,   a,       b
# bounds = [(0.01, 0.75), (0, 0.9), (0, 0.5), (0.1, 50), (0, 600), (0, 30), (2, 6), (0, 1)]



if estimate_parameters:
	print("New parameter estimation for {0} ...".format(exptitle))
	savesim = True
	ret = model.estimate(bounds,tuple(all_data))
	parameters = ret.x
	model.set_parameters(parameters)
	MSE = ret.fun
	# R2 = model.R2(parameters, tuple(all_data))
	# visi_parametri = model.parameter_table2(parameters=parameters, exptitle=exptitle, rmse=rmse, R2=R2, save=savesim)
else:
	print("New simulation of {0} ...".format(exptitle))
	parameters = model.load_parameters('parameters', row=paramrow)
	MSE = model.multi_fit_standalone(parameters, tuple(all_data))
	# R2 = model.R2(parameters, tuple(all_data))
	# print("R2 = {0}".format(R2))
	# visi_parametri = model.parameter_table2(parameters=parameters, exptitle=exptitle, rmse=rmse, R2=R2, save=savesim)


# print(visi_parametri.transpose())
mod_res_table = prepare_modeling_results(model,all_data,expnoList,path,exptitle)

# print(mod_res_table)

# Here we calculate separated correlation of biomass curve, etc.
XR2 = mySafeR2(mod_res_table['XnOBS'].values,mod_res_table['XnPRED'].values)
SR2 = mySafeR2(mod_res_table['SnOBS'].values,mod_res_table['SnPRED'].values)
PR2 = mySafeR2(mod_res_table['PnOBS'].values,mod_res_table['PnPRED'].values)

Xrmse = math.sqrt(mySafeMSE(mod_res_table['XnOBS'],mod_res_table['XnPRED']))
Srmse = math.sqrt(mySafeMSE(mod_res_table['SnOBS'],mod_res_table['SnPRED']))
Prmse = math.sqrt(mySafeMSE(mod_res_table['PnOBS'],mod_res_table['PnPRED']))

RMSE = math.sqrt(MSE)

R2 = model.R2(parameters, tuple(all_data))
visi_parametri = model.parameter_table2(parameters=parameters, exptitle=exptitle, rmse=RMSE, R2=R2, XR2=XR2, SR2=SR2, PR2=PR2, Xrmse=Xrmse, Srmse=Srmse, Prmse=Prmse, save=savesim)

visi_parametri.to_html("{0}parameters_{1}.html".format(path,exptitle))



plot_all_results(model,all_data,expnoList,path)

# plot_all_results_print(model,all_data,expnoList,path)
generate_simulation_results(model,all_data,expnoList,path,exptitle)

# plt.show()

# print("-------------------------")
process_desc = ("Parameter estimation" if estimate_parameters else "Simulation")
print("{0} of {1} finished".format(process_desc,exptitle))