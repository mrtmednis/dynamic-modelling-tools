#!/bin/bash
# Run estimation or simulation and notify when finished

starttime=$(date "+%H:%M")

echo "Modeling started $starttime"


# 120 
# 22-A1 25-B1

# 135
# 23-A2 26-B2

# 150
# 24-A3 27-B3




# 2016-03-15



# ============== LOO with Andrews (Zafar)


# echo " "
# echo "==== 1. started Andrews, J_laktoze batch $(date "+%H:%M")"
# # LOO J_laktoze
# ./modeler.py -estimate --model zafarLP --exptitle 'J_laktoze_LOO_120_135' --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py -estimate --model zafarLP --exptitle 'J_laktoze_LOO_120_150' --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py -estimate --model zafarLP --exptitle 'J_laktoze_LOO_135_150' --experiments 23-A2 26-B2 24-A3 27-B3

# # wait

# echo " "
# echo "==== 2. started Andrews, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py -estimate --model zafarLP --exptitle 'Inulins_LOO_150_200' --experiments 15-A1 18-B1
# ./modeler.py -estimate --model zafarLP --exptitle 'Inulins_LOO_80_200' --experiments 8-B1v 18-B1
# ./modeler.py -estimate --model zafarLP --exptitle 'Inulins_LOO_80_150' --experiments 8-B1v 15-A1

# # wait 

# echo " "
# echo "==== 3. started Andrews, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py -estimate --model zafarLP --exptitle 'V_laktoze_LOO_148_122' --experiments 12-L2 14-L4
# ./modeler.py -estimate --model zafarLP --exptitle 'V_laktoze_LOO_137_122' --experiments 11-L1 14-L4
# ./modeler.py -estimate --model zafarLP --exptitle 'V_laktoze_LOO_137_148' --experiments 11-L1 12-L2

# # wait

# # Validation
# # ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 43 --experiments 24-A3 27-B3
# # ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 42 --experiments 23-A2 26-B2
# # ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 44 --experiments 22-A1 25-B1


# # validacija
# # ./zafarLP.py --exptitle 'Inulins_LOOv_80' --paramrow 30 --experiments 8-B1v
# # ./zafarLP.py --exptitle 'Inulins_LOOv_150' --paramrow 31 --experiments 15-A1
# # ./zafarLP.py --exptitle 'Inulins_LOOv_200' --paramrow 32 --experiments 18-B1

# # validacija
# # ./model2.py --exptitle 'Laktoze_LOOv2_137' --paramrow 18 --path 'rezultati_model2/' --experiments 11-L1
# # ./model2.py --exptitle 'Laktoze_LOOv2_148' --paramrow 19 --path 'rezultati_model2/' --experiments 12-L2
# # ./model2.py --exptitle 'Laktoze_LOOv2_122' --paramrow 20 --path 'rezultati_model2/' --experiments 14-L4



# # ============== LOO with haldaneAndrewsLP

# echo " "
# echo "==== 4. started haldaneAndrewsLP, J_laktoze batch $(date "+%H:%M")"
# # LOO J_laktoze
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_120_135' --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_120_150' --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_135_150' --experiments 23-A2 26-B2 24-A3 27-B3


# # wait

# echo " "
# echo "==== 5. started haldaneAndrewsLP, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'Inulins_LOO_150_200' --experiments 15-A1 18-B1
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'Inulins_LOO_80_200' --experiments 8-B1v 18-B1
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'Inulins_LOO_80_150' --experiments 8-B1v 15-A1

# # wait 

# echo " "
# echo "==== 6. started haldaneAndrewsLP, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_148_122' --experiments 12-L2 14-L4
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_137_122' --experiments 11-L1 14-L4
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_137_148' --experiments 11-L1 12-L2

# # wait









# # ============== LOO with AibaEdwardsLP

# echo " "
# echo "==== 7. started AibaEdwardsLP, J_laktoze batch $(date "+%H:%M")"
# # LOO J_laktoze
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_120_135' --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_120_150' --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_135_150' --experiments 23-A2 26-B2 24-A3 27-B3


# # wait

# echo " "
# echo "==== 8. started AibaEdwardsLP, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'Inulins_LOO_150_200' --experiments 15-A1 18-B1
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'Inulins_LOO_80_200' --experiments 8-B1v 18-B1
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'Inulins_LOO_80_150' --experiments 8-B1v 15-A1

# # wait 

# echo " "
# echo "==== 9. started AibaEdwardsLP, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_148_122' --experiments 12-L2 14-L4
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_137_122' --experiments 11-L1 14-L4
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_137_148' --experiments 11-L1 12-L2

# # wait














# 2016-03-16

# LOO Validation


# echo " "
# echo "==== Simulating Andrews, $(date "+%H:%M")"

# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4



# echo " "
# echo "==== Simulating haldaneAndrewsLP, $(date "+%H:%M")"

# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4


# echo " "
# echo "==== Simulating AibaEdwardsLP, $(date "+%H:%M")"


# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4










# 2016-03-11 
# Today I check what results we get if we simulate industrial lactose and inulin with parameters from new lactose experiments


# ./modeler.py  --model zafarLP --exptitle 'Inulins_ar_J_lakt_param' -savesim --paramrow 4 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model zafarLP --exptitle 'V_laktoze__ar_J_lakt_param' -savesim --paramrow 4 --experiments 11-L1 12-L2 14-L4




# I discovered that my R2 implemetation from wikipedia and stackoverflow produces results (negative sometimes) that are not consistent with RSQ() from LibreOffice
# Therefore, I rerun simulations with already obtained parameters. RMSE is ok, no need to re-optimize again.


# # Simulation

# # Andrews
# ./modeler.py  --model zafarLP --exptitle 'Inulins' -savesim --paramrow 3 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model zafarLP --exptitle 'J_laktoze' -savesim --paramrow 4 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py  --model zafarLP --exptitle 'V_laktoze' -savesim --paramrow 5 --experiments 11-L1 12-L2 14-L4

# # haldaneAndrewsLP
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'Inulins' -savesim --paramrow 2 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'J_laktoze' -savesim --paramrow 3 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'V_laktoze' -savesim --paramrow 4 --experiments 11-L1 12-L2 14-L4

# # AibaEdwardsLP
# ./modeler.py  --model AibaEdwardsLP --exptitle 'Inulins' -savesim --paramrow 3 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model AibaEdwardsLP --exptitle 'J_laktoze' -savesim --paramrow 4 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py  --model AibaEdwardsLP --exptitle 'V_laktoze' -savesim --paramrow 5 --experiments 11-L1 12-L2 14-L4




# 2016-03-15
# Optimize again. I discovered wrongly written code for MSE calculation, hence re-optimization.

# # Andrews
# ./modeler.py -estimate --model zafarLP --exptitle 'Inulins' --experiments 8-B1v 15-A1 18-B1
# ./modeler.py -estimate --model zafarLP --exptitle 'J_laktoze' --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py -estimate --model zafarLP --exptitle 'V_laktoze' --experiments 11-L1 12-L2 14-L4

# # haldaneAndrewsLP
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'Inulins' --experiments 8-B1v 15-A1 18-B1
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'J_laktoze' --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'V_laktoze' --experiments 11-L1 12-L2 14-L4

# # AibaEdwardsLP
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'Inulins' --experiments 8-B1v 15-A1 18-B1
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'J_laktoze' --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'V_laktoze' --experiments 11-L1 12-L2 14-L4



# 2016-03-17
# Simulate inulin with J_laktoze parameters.

# # Simulation

# # # Andrews
# ./modeler.py  --model zafarLP --exptitle 'Inulins_ar_J_lakt_param' -savesim --paramrow 1 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model zafarLP --exptitle 'V_laktoze_ar_J_lakt_param' -savesim --paramrow 1 --experiments 11-L1 12-L2 14-L4

# # haldaneAndrewsLP
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'Inulins_ar_J_lakt_param' -savesim --paramrow 1 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'V_laktoze_ar_J_lakt_param' -savesim --paramrow 1 --experiments 11-L1 12-L2 14-L4

# # AibaEdwardsLP
# ./modeler.py  --model AibaEdwardsLP --exptitle 'Inulins_ar_J_lakt_param' -savesim --paramrow 1 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model AibaEdwardsLP --exptitle 'V_laktoze_ar_J_lakt_param' -savesim --paramrow 1 --experiments 11-L1 12-L2 14-L4



# Estimate parameters for J_laktoze and Inulin.

# # Andrews
# ./modeler.py -estimate --model zafarLP --exptitle 'Inulins_Jlaktoze' --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3 &

# # haldaneAndrewsLP
# ./modeler.py -estimate --model haldaneAndrewsLP --exptitle 'Inulins_Jlaktoze' --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3 &

# # AibaEdwardsLP
# ./modeler.py -estimate --model AibaEdwardsLP --exptitle 'Inulins_Jlaktoze' --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# wait

# validate for Inulin and for Lactose
# # # # Andrews
# ./modeler.py  --model zafarLP --exptitle 'Inulins_27' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model zafarLP --exptitle 'J_laktoze_27' -savesim --paramrow 27 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# # haldaneAndrewsLP
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'Inulins_27' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'J_laktoze_27' -savesim --paramrow 27 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# # AibaEdwardsLP
# ./modeler.py  --model AibaEdwardsLP --exptitle 'Inulins_27' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model AibaEdwardsLP --exptitle 'J_laktoze_27' -savesim --paramrow 27 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3


# # # # Andrews
# ./modeler.py  --model zafarLP --exptitle 'Inulins_0' -savesim --paramrow 0 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model zafarLP --exptitle 'J_laktoze_1' -savesim --paramrow 1 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# # haldaneAndrewsLP
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'Inulins_0' -savesim --paramrow 0 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model haldaneAndrewsLP --exptitle 'J_laktoze_1' -savesim --paramrow 1 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# # AibaEdwardsLP
# ./modeler.py  --model AibaEdwardsLP --exptitle 'Inulins_0' -savesim --paramrow 0 --experiments 8-B1v 15-A1 18-B1
# ./modeler.py  --model AibaEdwardsLP --exptitle 'J_laktoze_1' -savesim --paramrow 1 --experiments 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3

# wait for all subprocesses to finish





















# 2016-03-21
# Re-run all simulations to get the right RMSE values

# ============== LOO with Andrews (Zafar)


# echo " "
# echo "==== 1. started Andrews, J_laktoze batch $(date "+%H:%M")"
# # LOO J_laktoze
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOO_120_135' -savesim --paramrow 3 --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOO_120_150' -savesim --paramrow 4 --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOO_135_150' -savesim --paramrow 5 --experiments 23-A2 26-B2 24-A3 27-B3

# echo " "
# echo "==== 2. started Andrews, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOO_150_200' -savesim --paramrow 6 --experiments 15-A1 18-B1
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOO_80_200' -savesim --paramrow 7 --experiments 8-B1v 18-B1
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOO_80_150' -savesim --paramrow 8 --experiments 8-B1v 15-A1

# echo " "
# echo "==== 3. started Andrews, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOO_148_122' -savesim --paramrow 9 --experiments 12-L2 14-L4
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOO_137_122' -savesim --paramrow 10 --experiments 11-L1 14-L4
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOO_137_148' -savesim --paramrow 11 --experiments 11-L1 12-L2



# # ============== LOO with haldaneAndrewsLP

# echo " "
# echo "==== 4. started haldaneAndrewsLP, J_laktoze batch $(date "+%H:%M")"
# # LOO J_laktoze
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_120_135' -savesim --paramrow 3 --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_120_150' -savesim --paramrow 4 --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOO_135_150' -savesim --paramrow 5 --experiments 23-A2 26-B2 24-A3 27-B3

# echo " "
# echo "==== 5. started haldaneAndrewsLP, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOO_150_200' -savesim --paramrow 6 --experiments 15-A1 18-B1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOO_80_200' -savesim --paramrow 7 --experiments 8-B1v 18-B1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOO_80_150' -savesim --paramrow 8 --experiments 8-B1v 15-A1

# echo " "
# echo "==== 6. started haldaneAndrewsLP, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_148_122' -savesim --paramrow 9 --experiments 12-L2 14-L4
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_137_122' -savesim --paramrow 10 --experiments 11-L1 14-L4
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOO_137_148' -savesim --paramrow 11 --experiments 11-L1 12-L2

# # ============== LOO with AibaEdwardsLP

# echo " "
# echo "==== 7. started AibaEdwardsLP, J_laktoze batch $(date "+%H:%M")"
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_120_135' -savesim --paramrow 3 --experiments 22-A1 25-B1 23-A2 26-B2
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_120_150' -savesim --paramrow 4 --experiments 22-A1 25-B1 24-A3 27-B3
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOO_135_150' -savesim --paramrow 5 --experiments 23-A2 26-B2 24-A3 27-B3

# echo " "
# echo "==== 8. started AibaEdwardsLP, Inulins batch $(date "+%H:%M")"
# # LOO Inulins
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOO_150_200' -savesim --paramrow 6 --experiments 15-A1 18-B1
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOO_80_200' -savesim --paramrow 7 --experiments 8-B1v 18-B1
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOO_80_150' -savesim --paramrow 8 --experiments 8-B1v 15-A1

# echo " "
# echo "==== 9. started AibaEdwardsLP, V_laktoze batch $(date "+%H:%M")"
# # LOO V_laktoze
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_148_122' -savesim --paramrow 9 --experiments 12-L2 14-L4
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_137_122' -savesim --paramrow 10 --experiments 11-L1 14-L4
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOO_137_148' -savesim --paramrow 11 --experiments 11-L1 12-L2




# #### LOO Validation


# echo " "
# echo "==== Simulating Andrews, $(date "+%H:%M")"

# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model zafarLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model zafarLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model zafarLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4



# echo " "
# echo "==== Simulating haldaneAndrewsLP, $(date "+%H:%M")"

# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model haldaneAndrewsLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model haldaneAndrewsLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4


# echo " "
# echo "==== Simulating AibaEdwardsLP, $(date "+%H:%M")"


# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_150' -savesim --paramrow 3 --experiments 24-A3 27-B3
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_135' -savesim --paramrow 4 --experiments 23-A2 26-B2
# ./modeler.py --model AibaEdwardsLP --exptitle 'J_laktoze_LOOv_120' -savesim --paramrow 5 --experiments 22-A1 25-B1

# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_80' -savesim --paramrow 6 --experiments 8-B1v
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_150' -savesim --paramrow 7 --experiments 15-A1
# ./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_LOOv_200' -savesim --paramrow 8 --experiments 18-B1

# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_137' -savesim --paramrow 9 --experiments 11-L1
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_148' -savesim --paramrow 10 --experiments 12-L2
# ./modeler.py --model AibaEdwardsLP --exptitle 'V_laktoze_LOOv_122' -savesim --paramrow 11 --experiments 14-L4



# Simulation of joint J_laktoze and Inulin.

# Andrews
./modeler.py --model zafarLP --exptitle 'Inulins_Jlaktoze' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3 

# haldaneAndrewsLP
./modeler.py --model haldaneAndrewsLP --exptitle 'Inulins_Jlaktoze' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3 

# AibaEdwardsLP
./modeler.py --model AibaEdwardsLP --exptitle 'Inulins_Jlaktoze' -savesim --paramrow 27 --experiments 8-B1v 15-A1 18-B1 22-A1 25-B1 23-A2 26-B2 24-A3 27-B3



endtime=$(date "+%H:%M")
./notifier.py "Modeling finished" "Start: $starttime -- End: $endtime"

