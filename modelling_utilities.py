#!/usr/bin/python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series
from scipy import stats
import os.path
import math
import statistics



# plt.rcParams.update({'font.size': 12, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
plt.rcParams.update({'font.size': 12, 'font.family': 'sans-serif'})

def removenans(data, modelled):
	"""remove NaN values from measurements and corresponding modeled values"""
	nans = np.isnan(data)
	return np.compress(~nans,data), np.compress(~nans,modelled)

def load_experimental_data(sourceDataFrame,expnoList):
	df = sourceDataFrame
	all_data = list()
	for expno in expnoList:
		exp_data = df[df['id']==expno] # select the right experiment
		dataset = [exp_data['X'].values, exp_data['S'].values, exp_data['P'].values, exp_data['h'].values]
		all_data.append(dataset)
	return all_data

def mySafeR2(Y,Y_pred):
	""" Safe because it removes Nans."""
	# yi_list, fi_list = removenans(yi_list, fi_list) # here we don not skip first values, only nans
	Y = np.array(Y)
	Y_pred = np.array(Y_pred)
	Y, Y_pred = removenans(Y, Y_pred)
	slope, intercept, r_value, p_value, std_err = stats.linregress(Y,Y_pred)
	return r_value**2

def mySafeR2_obsolete(Y,Y_pred):
	""" Safe because it removes Nans."""
	# yi_list, fi_list = removenans(yi_list, fi_list) # here we don not skip first values, only nans
	yi_list, fi_list = removenans(Y, Y_pred)
	y_mean = sum(yi_list)/float(len(yi_list))
	ss_tot = sum((yi-y_mean)**2 for yi in yi_list)
	ss_err = sum((yi-fi)**2 for yi,fi in zip(yi_list,fi_list))
	r2 = 1 - (ss_err/ss_tot)
	return r2

def mySafeMSE(Y, Y_pred):
	""" Safe because it removes Nans and normalizes values. """
	y, y_pred = removenans(Y, Y_pred)
	y, y_pred = feature_scaling(y, y_pred)
	# y, y_pred = zero_mean_variance(y, y_pred)
	residual = np.sum( np.power(y - y_pred, 2) )
	return 1./(len(y)) * residual

def resc(a,r): # a is array, r is related array. The "r" should always be OBS data.
	"""Normalize (rescale) an array"""
	def gmax(*args):
		lmax = [] # local max values
		for l in args:
			lmax.append(max(l))
		return  max(lmax) # find global max value in the list of local max values

	def gmin(*args):
		lmin = [] # local max values
		for l in args:
			lmin.append(min(l))
		return  min(lmin) # find global max value in the list of local max values
	b = [] # empty list to store scaled values
	for i in a:
		if np.isnan(i): # deal with missing data
			b.append(np.nan)
		else:
			b.append( (i-gmin(a,r))/(gmax(a,r) - gmin(a,r)) ) #modeled and measured values the array have to be rescaled using the same min and max values
	return np.hstack(b) # create numpy array from list

def feature_scaling(Y, Y_pred):
	"""Normalize (rescale) an array"""

	Y = np.array(Y)
	Y_pred = np.array(Y_pred)

	minY = min(Y)
	maxY = max(Y)
	return (Y - minY)/(maxY - minY), (Y_pred - minY)/(maxY - minY)



def feature_scaling_safe(Y, Y_pred):
	"""Normalize (rescale) an array"""
	
	# y ,y_pred = removenans(Y, Y_pred)
	Y = np.array(Y)
	Y_pred = np.array(Y_pred)

	minY = min(y)
	maxY = max(y_pred)

	return (Y - minY)/(maxY - minY), (Y_pred - minY)/(maxY - minY)

def zero_mean_variance(Y, Y_pred):
	y, y_pred = removenans(Y, Y_pred)
	Deviation = math.sqrt(statistics.pvariance(y))
	Mean = statistics.mean(y)

	# print("Mean=={0}, Deviation=={1}".format(Mean,Deviation))

	Y = np.array(Y)
	Y_pred = np.array(Y_pred)

	# return y, y_pred
	return (Y - Mean)/Deviation, (Y_pred - Mean)/Deviation




def prepare_modeling_results(model,all_data,expnoList,path,exptitle):
	results_table = DataFrame(columns=['h','X','X model','S', 'S model', 'P', 'P model', 'expno', 'XnOBS', 'XnPRED', 'SnOBS', 'SnPRED', 'PnOBS', 'PnPRED'])
	for dataset1,expno1 in zip(all_data,expnoList):
		results_table1 = model.simulation(dataset1, expno=expno1)
		# calculate normalized values
		# XnOBS, XnPRED = feature_scaling(results_table1['X'].values,results_table1['X model'].values)
		# SnOBS, SnPRED = feature_scaling(results_table1['S'].values,results_table1['S model'].values)
		# PnOBS, PnPRED = feature_scaling(results_table1['P'].values,results_table1['P model'].values)
		# standardization
		XnOBS, XnPRED = zero_mean_variance(results_table1['X'].values,results_table1['X model'].values)
		SnOBS, SnPRED = zero_mean_variance(results_table1['S'].values,results_table1['S model'].values)
		PnOBS, PnPRED = zero_mean_variance(results_table1['P'].values,results_table1['P model'].values)

		
		# and add them to the table as new columns
		results_table1['XnOBS']  = XnOBS
		results_table1['XnPRED'] = XnPRED
		results_table1['SnOBS']  = SnOBS
		results_table1['SnPRED'] = SnPRED
		results_table1['PnOBS']  = PnOBS
		results_table1['PnPRED'] = PnPRED
		# now add the current experiment to the big table of all experiments
		results_table = results_table.append(results_table1)
	results_table.to_html("{0}results_table_{1}.html".format(path,exptitle))
	return results_table




def plot_all_results(model,all_data,expnoList,path):
	for dataset1,expno1 in zip(all_data,expnoList):
		last_time_point = dataset1[3][-1] + 0.1
		t = np.arange(0, last_time_point, 0.1) # smalkaak
		parameters = model.parameters
		results = model.solution(parameters, t, start_X=dataset1[0][0], start_S = dataset1[1][0], start_E=dataset1[2][0])
		plot_results(t,results,dataset1,expno=expno1, path=path)

def plot_all_results_print(model,all_data,expnoList,path):
	for dataset1,expno1 in zip(all_data,expnoList):
		last_time_point = dataset1[3][-1] + 0.1
		t = np.arange(0, last_time_point, 0.1) # smalkaak
		parameters = model.parameters
		results = model.solution(parameters, t, start_X=dataset1[0][0], start_S = dataset1[1][0], start_E=dataset1[2][0])
		plot_results_print(t,results,dataset1,expno=expno1, path=path)

def generate_simulation_results(model,all_data,expnoList,path,exptitle):
	results_table_h = DataFrame(columns=['h','X model','S model', 'P model', 'expno'])
	for dataset1,expno1 in zip(all_data,expnoList):
		last_time_point = dataset1[3][-1] + 0.1
		t = np.arange(0, last_time_point, 0.5) # smalkaak
		parameters = model.parameters
		modeled_values = model.solution(parameters, t, start_X=dataset1[0][0], start_S = dataset1[1][0], start_E=dataset1[2][0])
		modeled_values = np.transpose(modeled_values)
		results_table = DataFrame(modeled_values)
		results_table.columns = ['X model','S model','P model']

		results_table['h'] = t
		##round values
		results_table['X model'] = results_table['X model'].round(4)
		results_table['S model'] = results_table['S model'].round(4)
		results_table['P model'] = results_table['P model'].round(4)
		results_table['expno'] = expno1
		results_table_h = results_table_h.append(results_table)
	results_table_h = DataFrame(results_table_h, columns=['h','X model','S model', 'P model', 'expno'])
	results_table_h.to_html("{0}simulation_{1}.html".format(path,exptitle))




def mycorrelation(data_X,X):
	nans = np.isnan(data_X)
	data_X = np.compress(~nans,data_X)
	X = np.compress(~nans,X)
	return np.corrcoef(data_X,X)[0, 1]

def estiamte_miu(h,x):
	"""Estimate miu from experimental data"""
	all_miu = []
	for i in range(len(h)-1):
		# miu= (X2-X1)/(t2-t1) * 1/((X2+X1)/2)
		miu = ((x[i+1] - x[i])/(h[i+1] - h[i])) * 1./((x[i+1] + x[i])/2)
		all_miu.append( miu )
	all_miu = [np.nan] + all_miu
	return np.hstack(all_miu)

def estiamte_miu_high(h,x):
	all_miu = estiamte_miu(h,x)
	return max(all_miu[1:-1])

def estiamte_Yxs(x,s):
	dx = max(x)-min(x)
	ds = max(s)-min(s)
	return dx/ds

def estiamte_Yps(p,s):
	dp = max(p)-min(p)
	ds = max(s)-min(s)
	return dp/ds


def nice_charact(results_table1, **kwargs):
	"""Returns nicely formatted DataFrame with process characteristics calculated from experimental data"""
	miu_high = estiamte_miu_high(results_table1['h'].values, results_table1['X'].values)
	Yxs = estiamte_Yxs(results_table1['X'].values, results_table1['S'].values)
	Yps = estiamte_Yxs(results_table1['P'].values, results_table1['S'].values)

	data_ch = DataFrame({'Parameters':['miu_high', 'Yxs', 'Yps']})
	data_ch[kwargs['expno']] = [miu_high, Yxs, Yps]

	if 'path' in kwargs:
		# print("Saving requested...")
		if os.path.isfile(kwargs['path']+'.pickle'):
			# print("File exists...")
			all_data_ch = pd.read_pickle(kwargs['path']+'.pickle')
			all_data_ch[kwargs['expno']] = 0
			all_data_ch.drop(kwargs['expno'], axis=1, inplace=True)
			all_data_ch = all_data_ch.merge(data_ch, on='Parameters')
			all_data_ch.to_pickle(kwargs['path']+'.pickle')
			all_data_ch.to_html(kwargs['path']+'.html')
			# print("File saved...")
			return all_data_ch
		else:
			print("File for characteristics does not exist...creating a new one.")
			data_ch.to_pickle(kwargs['path']+'.pickle')
			data_ch.to_html(kwargs['path']+'.html')
			# print("New file saved...")
	# print("Saving none...")
	return data_ch


def plot_correlation(t,results,dataset1,results_table1, **kwargs):
	biomass, substrate, product = results
	fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(10,3))
	# miu = (miu_max * S) / (Ks + S + (np.power(S,2)/Ki) ) * Kx/(Kx+np.power(X,nX)) * Kip/(Kip+P) # Haldane + biomass inhibition

	## Correlation plots in the same window
	axes[0].plot([0,results_table1['X'].max()],[0,results_table1['X'].max()], color = '0.75')
	axes[0].plot(results_table1['X'],results_table1['X model'], "o")
	axes[0].set_xlabel("measured")
	axes[0].set_ylabel("modeled")
	# axes[0].set_title("Biomass")
	cor = round(mycorrelation(results_table1['X'],results_table1['X model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	axes[0].text(0.9, 0.1, r'Biomass $r^2=$'+format(cor), fontsize=12,transform=axes[0].transAxes, horizontalalignment='right', color=corcolor)
	
	axes[1].plot([0,results_table1['S'].max()],[0,results_table1['S'].max()], color = '0.75')
	axes[1].plot(results_table1['S'],results_table1['S model'], "o")
	# axes[1].set_title("Substrate")
	cor = round(mycorrelation(results_table1['S'],results_table1['S model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	axes[1].text(0.9, 0.1, r'Substrate $r^2=$'+format(cor), fontsize=12,transform=axes[1].transAxes, horizontalalignment='right', color=corcolor)

	axes[2].plot([0,results_table1['P'].max()],[0,results_table1['P'].max()], color = '0.75')
	axes[2].plot(results_table1['P'],results_table1['P model'], "o")
	# axes[2].set_title("Product")
	cor = round(mycorrelation(results_table1['P'],results_table1['P model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	axes[2].text(0.9, 0.1, r'Product $r^2=$'+format(cor), fontsize=12,transform=axes[2].transAxes, horizontalalignment='right', color=corcolor)

	initialsubstrate = "{0}".format(dataset1[1][0])
	if 'title' in kwargs:
		fig.suptitle(kwargs['title']+', S='+initialsubstrate+' - '+kwargs['expno'], fontsize=18)
	else:
		fig.suptitle(kwargs['expno']+', S='+initialsubstrate, fontsize=18)

	fig.tight_layout()
	if 'path' in kwargs:
		plt.savefig(kwargs['path']+'_'+kwargs['expno']+'.png',dpi=100)
	pass


# def plot_results(t,results,dataset1,results_table1, **kwargs):
def plot_results(t,results,dataset1, **kwargs):
	biomass, substrate, product = results

	fig = plt.figure(figsize=(10,8))
	ax0 = plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=2)
	ax1 = plt.subplot2grid((3,3), (2,0))
	ax2 = plt.subplot2grid((3,3), (2,1))
	ax3 = plt.subplot2grid((3,3), (2,2))

	# ax4 = plt.subplot2grid((4,3), (3,0))
	# ax5 = plt.subplot2grid((4,3), (3,1))
	# ax6 = plt.subplot2grid((4,3), (3,2))



	ax0.plot(t, biomass, 'g-', label='X model')
	ax0.plot(t, substrate, 'y-', label='S model')
	ax0.plot(t, product, 'b-', label='P model')

	ax0.plot(dataset1[3], dataset1[0], 'go', label='X data', markersize=8, alpha=0.8)
	ax0.plot(dataset1[3], dataset1[1], 'ys', label='S data', markersize=8, alpha=0.8)
	ax0.plot(dataset1[3], dataset1[2], 'b^', label='P data', markersize=8, alpha=0.8)
	ax0.set_xlabel("hours")
	ax0.set_ylabel("g/l")
	ax0.grid(True)
	ax0.legend(loc='best', fontsize=10)
	
	# Small biomass plot
	ax1.set_title("Biomass conc.", fontsize=10)
	ax1.plot(dataset1[3], dataset1[0], 'go', label='Biomass data', markersize=3, alpha=0.8)
	ax1.plot(t, biomass, 'g-', label='Biomass')
	ax1.set_xlabel("hours", fontsize=10)
	ax1.set_ylabel('g/l', fontsize=10)


	miu_data = estiamte_miu(dataset1[3],dataset1[0])
	miu_model= estiamte_miu(t,biomass)
	ax2.set_title(r'$\mu/t$')
	ax2.plot(dataset1[3], miu_data, ls=':',marker='o', color="purple", label='Mi data', markersize=3, alpha=0.8)
	ax2.plot(t, miu_model, ls='-', color="purple", label='Mi model')
	ax2.set_xlabel("hours", fontsize=10)
	ax2.set_ylabel(r'$\mu$', fontsize=10)

	ax3.set_title(r'$\mu/S$')
	ax3.plot(dataset1[1], miu_data, ls=':',marker='o', color="purple", label='Mi data', markersize=3, alpha=0.8)
	ax3.plot(substrate, miu_model, ls='-', color="purple", label='Mi model')
	ax3.set_xlabel("Substrate g/L", fontsize=10)
	ax3.set_ylabel(r'$\mu$', fontsize=10)

	ax1.grid(True)
	ax2.grid(True)
	ax3.grid(True)


	# correlation plots


	# ax4.plot([0,results_table1['X'].max()],[0,results_table1['X'].max()], color = '0.75')
	# ax4.plot(results_table1['X'],results_table1['X model'], "o")
	# ax4.set_xlabel("measured")
	# ax4.set_ylabel("modeled")
	# # axes[0].set_title("Biomass")
	# cor = round(mycorrelation(results_table1['X'],results_table1['X model']),4)
	# corcolor = ('red' if (cor<0.97) else 'black')
	# ax4.text(0.9, 0.1, r'Biomass $r^2=$'+format(cor), fontsize=12,transform=ax4.transAxes, horizontalalignment='right', color=corcolor)
	
	# ax5.plot([0,results_table1['S'].max()],[0,results_table1['S'].max()], color = '0.75')
	# ax5.plot(results_table1['S'],results_table1['S model'], "o")
	# # axes[1].set_title("Substrate")
	# cor = round(mycorrelation(results_table1['S'],results_table1['S model']),4)
	# corcolor = ('red' if (cor<0.97) else 'black')
	# ax5.text(0.9, 0.1, r'Substrate $r^2=$'+format(cor), fontsize=12,transform=ax5.transAxes, horizontalalignment='right', color=corcolor)

	# ax6.plot([0,results_table1['P'].max()],[0,results_table1['P'].max()], color = '0.75')
	# ax6.plot(results_table1['P'],results_table1['P model'], "o")
	# # axes[2].set_title("Product")
	# cor = round(mycorrelation(results_table1['P'],results_table1['P model']),4)
	# corcolor = ('red' if (cor<0.97) else 'black')
	# ax6.text(0.9, 0.1, r'Product $r^2=$'+format(cor), fontsize=12,transform=ax6.transAxes, horizontalalignment='right', color=corcolor)







	initialsubstrate = "{0}".format(dataset1[1][0])
	if 'title' in kwargs:
		fig.suptitle(kwargs['title']+', S='+initialsubstrate+' - '+kwargs['expno'], fontsize=18)
	else:
		fig.suptitle(kwargs['expno']+', S='+initialsubstrate, fontsize=18)

	fig.tight_layout()
	if 'path' in kwargs:
		plt.savefig(kwargs['path']+'plot_'+kwargs['expno']+'.png',dpi=100)
	pass




def plot_results_print(t,results,dataset1, **kwargs):
	biomass, substrate, product = results

	# fig = plt.figure(figsize=(10,8))
	fig, ax0 = plt.subplots(figsize=(10,8))
	ax1 = ax0.twinx()
	# ax0 = plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=2)
	# ax1 = plt.subplot2grid((3,3), (2,0))
	# ax2 = plt.subplot2grid((3,3), (2,1))
	# ax3 = plt.subplot2grid((3,3), (2,2))

	ax0.set_ylim([0, dataset1[1][0]+5])
	
	line_X, = ax1.plot(t, biomass, color='green', ls='--', lw=2, label='Biomass, model')
	line_S, = ax0.plot(t, substrate, color='#a08500', ls='-.', lw=4, label='Substrate, model')
	dashes = [3, 5, 3, 5] # 10 points on, 5 off, 10 on, 5 off
	line_S.set_dashes(dashes)
	
	line_P, = ax1.plot(t, product, color='blue', ls='-', lw=3, label='Ethanol, model')

	


	line_Xm, = ax1.plot(dataset1[3], dataset1[0], color='green', ls='', marker='o', label='Biomass, experiment', markersize=10, alpha=0.8, markerfacecolor="white", markeredgewidth=2, markeredgecolor="green")
	
	ax1.set_ylabel(r"Biomass, product concentration $(g/l)$", fontsize=18, color="green")

	line_Sm, = ax0.plot(dataset1[3], dataset1[1], color='#a08500', ls='', marker='s', label='Substrate, experiment', markersize=10, alpha=0.8, markerfacecolor="white", markeredgewidth=2, markeredgecolor="#a08500")
	line_Pm, = ax1.plot(dataset1[3], dataset1[2], color='blue', ls='', marker='^', label='Ethanol, experiment', markersize=10, alpha=0.8, markerfacecolor="white", markeredgewidth=2, markeredgecolor="blue")
	
	ax0.set_ylabel(r"Substrate concentration $(g/l)$", fontsize=18)

	ax0.set_xlabel("hours")
	# ax0.set_ylabel("g/l")
	ax0.grid(True)
	# ax0.legend(loc='best', fontsize=10)
	# ax0.legend(bbox_to_anchor=(0., 1.12, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
	# ax1.legend(loc='best', fontsize=10)
	# ax1.legend(bbox_to_anchor=(0., 1.12, 1., .205), loc=3, ncol=2, mode="expand", borderaxespad=0.)
	
	# Create a legend for the first line.
	legend1 = plt.legend(handles=[line_X, line_Xm, line_S, line_Sm, line_P, line_Pm], loc=0, fontsize=12, bbox_to_anchor=(0., 1.02, 1., .102), ncol=3)

	# Add the legend manually to the current Axes.
	# ax0 = plt.gca().add_artist(legend1)

	# Create another legend for the second line.
	# legend2 = plt.legend(handles=[line_S], loc=1)
	# plt.legend(handles=[line_Sm], loc=0)
	
	# ax0 = plt.gca().add_artist(legend2)

	# legend3 = plt.legend(handles=[line_P], loc=0)
	# # plt.legend(handles=[line_Sm], loc=0)
	
	# ax0 = plt.gca().add_artist(legend3)

	# ax0.grid(True)
	# ax1.grid(True)
	




	# initialsubstrate = "{0}".format(dataset1[1][0])
	# if 'title' in kwargs:
	# 	fig.suptitle(kwargs['title']+', S='+initialsubstrate+' - '+kwargs['expno'], fontsize=18)
	# else:
	# 	fig.suptitle(kwargs['expno']+', S='+initialsubstrate, fontsize=18)

	# fig.tight_layout()
	if 'path' in kwargs:
		plt.savefig(kwargs['path']+'print_plot_'+kwargs['expno']+'.png',dpi=100)
	pass







def plot_results4(t,results,dataset1,results_table1, **kwargs):
	biomass, substrate, product1, product2 = results

	fig = plt.figure(figsize=(10,10))
	ax0 = plt.subplot2grid((4,3), (0,0), colspan=3, rowspan=2)
	ax1 = plt.subplot2grid((4,3), (2,0))
	ax2 = plt.subplot2grid((4,3), (2,1))
	ax3 = plt.subplot2grid((4,3), (2,2))

	ax4 = plt.subplot2grid((4,3), (3,0))
	ax5 = plt.subplot2grid((4,3), (3,1))
	ax6 = plt.subplot2grid((4,3), (3,2))



	ax0.plot(t, biomass, 'g-', label='X model')
	ax0.plot(t, substrate, 'y-', label='S model')
	ax0.plot(t, product1, 'b-', label='P1 model')
	ax0.plot(t, product2, 'r-', label='P2 model')

	ax0.plot(dataset1[0], dataset1[1], 'go', label='X data', markersize=8, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[2], 'ys', label='S data', markersize=8, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[3], 'b^', label='P1 data', markersize=8, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[4], 'r^', label='P2 data', markersize=8, alpha=0.8)
	ax0.set_xlabel("hours")
	ax0.set_ylabel("g/l")
	ax0.grid(True)
	ax0.legend(loc='best', fontsize=10)
	
	# Small biomass plot
	ax1.set_title("Biomass conc.", fontsize=10)
	ax1.plot(dataset1[0], dataset1[1], 'go', label='Biomass data', markersize=3, alpha=0.8)
	ax1.plot(t, biomass, 'g-', label='Biomass')
	ax1.set_xlabel("hours", fontsize=10)
	ax1.set_ylabel('g/l', fontsize=10)


	miu_data = estiamte_miu(dataset1[0],dataset1[1])
	miu_model= estiamte_miu(t,biomass)
	ax2.set_title(r'$\mu/t$')
	ax2.plot(dataset1[0], miu_data, ls=':',marker='o', color="purple", label='Mi data', markersize=3, alpha=0.8)
	ax2.plot(t, miu_model, ls='-', color="purple", label='Mi model')
	ax2.set_xlabel("hours", fontsize=10)
	ax2.set_ylabel(r'$\mu$', fontsize=10)

	ax3.set_title(r'$\mu/S$')
	ax3.plot(dataset1[2], miu_data, ls=':',marker='o', color="purple", label='Mi data', markersize=3, alpha=0.8)
	ax3.plot(substrate, miu_model, ls='-', color="purple", label='Mi model')
	ax3.set_xlabel("Substrate g/L", fontsize=10)
	ax3.set_ylabel(r'$\mu$', fontsize=10)

	ax1.grid(True)


	# correlation plots


	ax4.plot([0,results_table1['X'].max()],[0,results_table1['X'].max()], color = '0.75')
	ax4.plot(results_table1['X'],results_table1['X model'], "o")
	ax4.set_xlabel("measured")
	ax4.set_ylabel("modeled")
	# axes[0].set_title("Biomass")
	cor = round(mycorrelation(results_table1['X'],results_table1['X model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	ax4.text(0.9, 0.1, r'Biomass $r^2=$'+format(cor), fontsize=12,transform=ax4.transAxes, horizontalalignment='right', color=corcolor)
	
	ax5.plot([0,results_table1['S'].max()],[0,results_table1['S'].max()], color = '0.75')
	ax5.plot(results_table1['S'],results_table1['S model'], "o")
	# axes[1].set_title("Substrate")
	cor = round(mycorrelation(results_table1['S'],results_table1['S model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	ax5.text(0.9, 0.1, r'Substrate $r^2=$'+format(cor), fontsize=12,transform=ax5.transAxes, horizontalalignment='right', color=corcolor)

	ax6.plot([0,results_table1['P1'].max()],[0,results_table1['P1'].max()], color = '0.75')
	ax6.plot(results_table1['P1'],results_table1['P1 model'], "o")
	# axes[2].set_title("Product")
	cor = round(mycorrelation(results_table1['P1'],results_table1['P1 model']),4)
	corcolor = ('red' if (cor<0.97) else 'black')
	ax6.text(0.9, 0.1, r'Product $r^2=$'+format(cor), fontsize=12,transform=ax6.transAxes, horizontalalignment='right', color=corcolor)



	initialsubstrate = "{0}".format(dataset1[2][0])
	if 'title' in kwargs:
		fig.suptitle(kwargs['title']+', S='+initialsubstrate+' - '+kwargs['expno'], fontsize=18)
	else:
		fig.suptitle(kwargs['expno']+', S='+initialsubstrate, fontsize=18)

	fig.tight_layout()
	if 'path' in kwargs:
		plt.savefig(kwargs['path']+'_'+kwargs['expno']+'.png',dpi=100)
	pass





def plot_results4_print(t,results,dataset1,results_table1, **kwargs):
	biomass, substrate, product1, product2 = results
	initialsubstrate = "{0}".format(dataset1[2][0])
	
	fig, ax0 = plt.subplots(figsize=(10,8))

	ax0.plot(t, biomass, color='green', ls='--', lw=2, label='Biomass, model')
	line, = ax0.plot(t, substrate, color='#a08500', ls='-.', lw=4, label='Substrate, model')
	ax0.plot(t, product1, color='blue', ls='-', lw=3, label='Product1, model')
	ax0.plot(t, product2, color='red', ls='-', lw=3, label='Product2, model')

	dashes = [3, 5, 3, 5] # 10 points on, 5 off, 10 on, 5 off
	line.set_dashes(dashes)

	ax0.plot(dataset1[0], dataset1[1], color='green', ls='', marker='o', label='Biomass, experiment', markersize=10, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[2], color='#a08500', ls='', marker='s', label='Substrate, experiment', markersize=10, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[3], color='blue', ls='', marker='^', label='Product1, experiment', markersize=10, alpha=0.8)
	ax0.plot(dataset1[0], dataset1[4], color='red', ls='', marker='^', label='Product2, experiment', markersize=10, alpha=0.8)

	ax0.set_xlabel("hours")
	ax0.set_ylabel("g/l")
	# ax0.grid(True)
	ax0.legend(loc='best', fontsize=14)

	# ax0.set_ylim([0, 60])
	ax0.set_ylim([0, dataset1[2][0]+5])
	ax0.set_xlim([0, t[-1]+1])
	
	if 'title' in kwargs:
		pass
		# fig.suptitle(kwargs['title']+', S='+initialsubstrate+' - '+kwargs['expno'], fontsize=18)
	else:
		pass
		# fig.suptitle(kwargs['expno']+', S='+initialsubstrate, fontsize=18)

	

	fig.tight_layout()
	if 'path' in kwargs:
		plt.savefig(kwargs['path']+'_'+kwargs['expno']+'.png',dpi=90)
		plt.savefig(kwargs['path']+'_'+kwargs['expno']+'.pdf')
	pass


