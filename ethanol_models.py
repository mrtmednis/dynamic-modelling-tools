#!/usr/bin/python
# -*- coding: utf-8 -*-

import os.path

from scipy import integrate

import numpy as np

from dynamicmodelclass import dynamicmodel3

import pandas as pd
from pandas import DataFrame, Series
from modelling_utilities import *



class poletto(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['K1', 'K2', 'Yps', 'Yxs', 'Kip', 'Emax', 'Ks']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		def f(initial, t, K1,K2,Yps,Yxs,Kip,Emax,Ks):
			X = initial[0]
			S = initial[1]
			P = initial[2]
			
			a = np.exp((P-Emax)*Kip)
			b = (X/(Ks+S))
			c = S*(K1*(1-a)+K2*a)

			dX =     b * c * Yxs
			dS = -1.*b * c
			dP =     b * (1-a) * (Yps*K1*S)

			return np.array([dX,dS,dP], dtype=float)
		K1,K2,Yps,Yxs,Kip,Emax,Ks = beta

		X0 = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)
		# X0 = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, X0, t, args=(K1,K2,Yps,Yxs,Kip,Emax,Ks))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)




class ariyanti(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Ks', 'Yxs', 'a', 'b']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		def f(initial, t, miu_max, Ks, Yxs, a, b):
			X = initial[0]
			S = initial[1]
			P = initial[2]
			miu = miu_max * (S/(Ks+S))
			dX = miu * X
			dS = -miu * (X/Yxs)
			dP = a * dX + b * X
			return np.array([dX,dS,dP], dtype=float)
		miu_max, Ks, Yxs, a, b = beta

		X0 = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)
		# X0 = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, X0, t, args=(miu_max, Ks, Yxs, a, b))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)



class zafar(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Yxs', 'Yps', 'mS', 'Ks', 'Kis', 'Kip']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		"""Zafar lactose model"""
		# all_miu = []
		def f(initial, t, miu_max, Yxs, Yps, mS, Ks, Kis, Kip):
			X = initial[0]
			S = initial[1]
			P = initial[2]

			miu = miu_max* (S/(Ks+S)) * (1/(1+S/Kis)) * (Kip/(Kip+P))
			dX = miu * X
			dS = -1.*((miu/Yxs) * X + mS*X)
			dP = Yps/Yxs * miu * X

			return np.array([dX,dS,dP], dtype=float)

		miu_max, Yxs, Yps, mS, Ks, Kis, Kip = beta

		# initial = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)
		initial = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, initial, t, args=(miu_max, Yxs, Yps, mS, Ks, Kis, Kip))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)
		


class zafarLP(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Yxs', 'mS', 'Ks', 'Kis', 'Kip', 'a', 'b']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		"""Zafar lactose model"""
		# all_miu = []
		def f(initial, t, miu_max, Yxs, mS, Ks, Kis, Kip, a, b):
			X = initial[0]
			S = initial[1]
			P = initial[2]

			miu = miu_max* (S/(Ks+S)) * (1/(1+S/Kis)) * (Kip/(Kip+P))
			dX = miu * X
			dS = -1.*((miu/Yxs) * X + mS*X)
			dP = a * dX + b * X

			return np.array([dX,dS,dP], dtype=float)

		miu_max, Yxs, mS, Ks, Kis, Kip, a, b = beta

		# initial = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)
		initial = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, initial, t, args=(miu_max, Yxs, mS, Ks, Kis, Kip, a, b))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)


class haldaneAndrewsLP(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Yxs', 'mS', 'Ks', 'Kis', 'Kip', 'a', 'b']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		"""dX, and dS from Zafar, miu is NC2, dP from Ariyanti"""
		# all_miu = []
		def f(initial, t, miu_max, Yxs, mS, Ks, Kis, Kip, a, b):
			X = initial[0]
			S = initial[1]
			P = initial[2]

			miu = (miu_max * S) / (Ks + S + (np.power(S,2)/Kis) ) * Kip/(Kip+P) # Haldane + product inhibition

			dX = miu * X
			dS = -1.*((miu/Yxs) * X + mS*X)
			dP = a * dX + b * X

			return np.array([dX,dS,dP], dtype=float)

		miu_max, Yxs, mS, Ks, Kis, Kip, a, b = beta

		# initial = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)
		initial = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, initial, t, args=(miu_max, Yxs, mS, Ks, Kis, Kip, a, b))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)


class model2(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Yxs', 'mS', 'Ks', 'Kis', 'Kip', 'a', 'b']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		"""dX, and dS from Zafar, miu is NC1, dP from Ariyanti"""
		# all_miu = []
		def f(initial, t, miu_max, Yxs, mS, Ks, Kis, Kip, a, b):
			X = initial[0]
			S = initial[1]
			P = initial[2]

			miu=(miu_max)/((1+Ks/S) * (1+S/Kis)) * Kip/(Kip+P) # noncompetitive1 + product inhibition

			dX = miu * X
			dS = -1.*((miu/Yxs) * X + mS*X)
			dP = a * dX + b * X

			return np.array([dX,dS,dP], dtype=float)

		miu_max, Yxs, mS, Ks, Kis, Kip, a, b = beta

		# initial = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)
		initial = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, initial, t, args=(miu_max, Yxs, mS, Ks, Kis, Kip, a, b))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)

class AibaEdwardsLP(dynamicmodel3):
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.paramaters_names = ['miu_max', 'Yxs', 'mS', 'Ks', 'Kis', 'Kip', 'a', 'b']
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.expno = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False

	def solution(self, beta, t, **kwargs):
		"""dX, and dS from Zafar, miu is NC2, dP from Ariyanti"""
		# all_miu = []
		def f(initial, t, miu_max, Yxs, mS, Ks, Kis, Kip, a, b):
			X = initial[0]
			S = initial[1]
			P = initial[2]

			miu = miu_max*S/(Ks+S) * np.exp(-1.*S/Kis) * Kip/(Kip+P)

			dX = miu * X
			dS = -1.*((miu/Yxs) * X + mS*X)
			dP = a * dX + b * X

			return np.array([dX,dS,dP], dtype=float)

		miu_max, Yxs, mS, Ks, Kis, Kip, a, b = beta

		# initial = np.array([start_X, kwargs['start_S'], kwargs['start_E']], dtype=float)
		initial = np.array([kwargs['start_X'], kwargs['start_S'], kwargs['start_E']], dtype=float)

		X_calculated = integrate.odeint(f, initial, t, args=(miu_max, Yxs, mS, Ks, Kis, Kip, a, b))
		self.results = np.transpose(X_calculated)
		return np.transpose(X_calculated)
