#!/usr/bin/python
# -*- coding: utf-8 -*-

import os.path
from scipy import integrate
import numpy as np
from scipy.optimize import differential_evolution
import pandas as pd
from pandas import DataFrame, Series
from modelling_utilities import *
import datetime
# from sklearn.metrics import r2_score

class dynamicmodel:
	def __init__(self, name, **kwargs):
		self.name = name
		self.bounds = []
		self.results = []
		self.parameters =''
		self.paramaters_names = []
		if 'path' in kwargs:
			self.path = kwargs['path']
		else:
			self.path = False
		if 'exptitle' in kwargs:
			self.exptitle = kwargs['exptitle']
		else:
			self.exptitle = 'None'
		if 'save' in kwargs:
			self.save = True
		else:
			self.save = False
	
	def solution(self, beta, t, **kwargs):
		def f(initial, t, miu_max):
			pass
		pass

	def fit(self, beta, data_X, data_S, data_E, data_t):
		pass

	def multi_fit(self, beta, *args):
		pass

	def estimate(self,bounds,args):
		ret = differential_evolution(self.multi_fit, bounds, args, popsize=100)
		self.parameters = ret.x
		return ret	

	def load_parameters(self, filename, **kwargs):
		"""Loads parameters for the same experiment with the smallest RMSE"""
		df = pd.read_pickle("{0}{1}.pickle".format(self.path,filename))
		if kwargs['row'] > -1:
			best_index = kwargs['row']
			best_row = df.ix[best_index]
		else:
			frame = df[df['exptitle']==self.exptitle] # select the right experiment
			best_index = frame['rmse'].argmin() # find parameters that give the smallest RMSE
			# print(frame.ix[best_index])
			best_row = frame.ix[best_index]
		
		# print(best_row.values[0:len(self.paramaters_names)])
		self.parameters = best_row.values[0:len(self.paramaters_names)]
		return self.parameters

	def set_parameters(self, parameters):
		self.parameters = parameters
		pass

	def parameter_table(self, **kwargs):
		"""Returns nicely formatted DataFrame with parameters from all previous experiments"""
		parameter_table = DataFrame({'Parameters':self.paramaters_names})

		if 'parameters' in kwargs:
			self.parameters = kwargs['parameters']
			parameter_table[self.expno] = self.parameters
		else:
			parameter_table[self.expno] = self.parameters
		
		if 'bounds' in kwargs:
			parameter_table[self.expno+' bounds'] = ["{0}".format(b) for b in kwargs['bounds']]

		parameter_table.index = parameter_table['Parameters'].values
		if self.path:
			filename = 'parameters'
			appended_text = ''
			if 'rmse' in kwargs:
				filename = "{0}_{1}".format(filename,kwargs['rmse'])
				appended_text = "\n\n<p>RMSE: {0}</p>\n\n".format(kwargs['rmse'])
			# print("Saving requested...")
			if os.path.isfile(self.path+'parameters.pickle'):
				# print("File exists...")

				all_parameters = pd.read_pickle(self.path+'parameters.pickle')
				all_parameters[self.expno] = 0
				all_parameters.drop(self.expno, axis=1, inplace=True)
				all_parameters[self.expno+' bounds'] = 0
				all_parameters.drop(self.expno+' bounds', axis=1, inplace=True)
				all_parameters = all_parameters.merge(parameter_table, on='Parameters')
				all_parameters.to_html("{0}{1}_r.html".format(self.path,filename))
				all_parameters.to_pickle("{0}{1}_r.pickle".format(self.path,filename))
				# append RMSE to html file
				with open("{0}{1}.html".format(self.path,filename), "a") as myfile:
					myfile.write(appended_text)


				# print("File saved...")
				return all_parameters
			else:
				print("File for parameters does not exist...creating a new one.")
				parameter_table.to_pickle(self.path+'parameters.pickle')
				parameter_table.to_html(self.path+'parameters.html')
				# print("New file saved...")

		# print("Saving none...")
		return parameter_table

	def parameter_table2(self, **kwargs):
		"""Returns nicely formatted DataFrame with parameters from all previous experiments"""
		mycolumns = self.paramaters_names
		mycolumns.extend(('exptitle','rmse','R2','time','XR2','SR2', 'PR2', 'Xrmse', 'Srmse', 'Prmse'))
		parameter_table = DataFrame(columns=mycolumns)
		

		if 'parameters' in kwargs:
			self.parameters = kwargs['parameters']

		rmse = (kwargs['rmse'] if 'rmse' in kwargs else 'NaN')
		R2 = (kwargs['R2'] if 'R2' in kwargs else 'NaN')
		XR2 = (kwargs['XR2'] if 'XR2' in kwargs else 'NaN')
		SR2 = (kwargs['SR2'] if 'SR2' in kwargs else 'NaN')
		PR2 = (kwargs['PR2'] if 'PR2' in kwargs else 'NaN')

		Xrmse = (kwargs['Xrmse'] if 'Xrmse' in kwargs else 'NaN')
		Srmse = (kwargs['Srmse'] if 'Srmse' in kwargs else 'NaN')
		Prmse = (kwargs['Prmse'] if 'Prmse' in kwargs else 'NaN')
			
		row = list(self.parameters)
		row.extend((self.exptitle,rmse, R2, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), XR2, SR2, PR2, Xrmse, Srmse, Prmse ))
		
		parameter_table.loc[len(parameter_table)] = row # append row

		if 'save' in kwargs and kwargs['save'] == True:
			if self.path:
				filename = 'parameters'
				if os.path.isfile("{0}{1}.pickle".format(self.path,filename)):
					# print("File exists...")
					all_parameters = pd.read_pickle("{0}{1}.pickle".format(self.path,filename))
					# all_parameters.append(parameter_table)
					
					all_parameters.loc[len(all_parameters)] = row # append row

					all_parameters.to_html("{0}{1}.html".format(self.path,filename))
					all_parameters.to_pickle("{0}{1}.pickle".format(self.path,filename))
				else:
					# print("File for parameters does not exist...creating a new one.")
					parameter_table.to_html("{0}{1}.html".format(self.path,filename))
					parameter_table.to_pickle("{0}{1}.pickle".format(self.path,filename))
					# print("New file saved...")
		return parameter_table





class dynamicmodel3(dynamicmodel):
	"""Class for models with 3 concentrations"""
	def simulate_all_experiments(self, beta, *args):
		"""dataset[0]: biomass, 
		dataset[1]: substrate, 
		dataset[2]: product, 
		dataset[3]: time"""

		args = list(map(list, args))
		nOBS = []
		nPRED = []

		for dataset in args[0]:
			# print("Parameters: {0}".format(beta))
			# print "Time",dataset[3]
			data_X = dataset[0]
			data_S = dataset[1]
			data_E = dataset[2]
			data_t = dataset[3]
			# Well, this is wrong. We should concatenate all results and then compute MSE
			# fitness = self.fit(beta, dataset[0], dataset[1], dataset[2], dataset[3])
			# fitness_all.append(fitness)

			result = self.solution(beta,data_t, start_X=data_X[0], start_S=data_S[0], start_E=data_E[0])
			# this is the right place to skip first values
			data_X = data_X[1:]
			X = result[0][1:]
			data_S = data_S[1:]
			S = result[1][1:]
			data_E = data_E[1:]
			E = result[2][1:]

			# we may not normalize nor standardize here. We must normalize only after the concatenation.
			
			nOBS = nOBS + data_X.tolist() + data_S.tolist() + data_E.tolist()
			nPRED = nPRED + X.tolist() + S.tolist() + E.tolist()
		return nOBS, nPRED



	def fit(self, beta, data_X, data_S, data_E, data_t):
		"""Objective function for this model"""	
		# start_X, miu_max, Yxs, Yps, mS, Ks, Kis, Kip = beta
		result = self.solution(beta,data_t, start_X=data_X[0], start_S=data_S[0], start_E=data_E[0])
		
		## remove NaN values
		data_X, X = removenans(data_X, result[0])
		data_S, S = removenans(data_S, result[1])
		data_E, E = removenans(data_E, result[2])

		# normalize - Feature scaling
		data_X, X = feature_scaling(data_X, X)
		data_S, S = feature_scaling(data_S, S)
		data_E, E= feature_scaling(data_E, E)

		# standardize - zero-mean and unit-variance
		# data_X, X = zero_mean_variance(data_X, X)
		# data_S, S = zero_mean_variance(data_S, S)
		# data_E, E = zero_mean_variance(data_E, E)


		residX = np.sum( np.power(data_X - X, 2) )
		residS = np.sum( np.power(data_S - S, 2) )
		residE = np.sum( np.power(data_E - E, 2) )
		resid = residX + residS + residE

		return 1./(len(data_X) + len(data_S) + len(data_E)) * resid

	def no_use_multi_fit(self, beta, *args):
		"""dataset[0]: biomass, 
		dataset[1]: substrate, 
		dataset[2]: product, 
		dataset[3]: time"""
		
		fitness_all = [] # local max values

		# print(args)
		for dataset in args:
			# print("Parameters: {0}".format(beta))
			# print "Time",dataset[3]
			fitness = self.fit(beta, dataset[0], dataset[1], dataset[2], dataset[3])
			fitness_all.append(fitness)


		return np.sum(fitness_all)

	def multi_fit_standalone(self, beta, *args):
		"""dataset[0]: biomass, 
		dataset[1]: substrate, 
		dataset[2]: product, 
		dataset[3]: time"""
		
		# fitness_all = [] # local max values

		# args = list(args)
		# args = map(list, args)
		args = list(map(list, args))

		nOBS = []
		nPRED = []

		for dataset in args[0]:
			# print("Parameters: {0}".format(beta))
			# print "Time",dataset[3]
			data_X = dataset[0]
			data_S = dataset[1]
			data_E = dataset[2]
			data_t = dataset[3]
			# Well, this is wrong. We should concatenate all results and then compute MSE
			# fitness = self.fit(beta, dataset[0], dataset[1], dataset[2], dataset[3])
			# fitness_all.append(fitness)

			result = self.solution(beta,data_t, start_X=data_X[0], start_S=data_S[0], start_E=data_E[0])
			# this is the right place to skip first values
			data_X = data_X[1:]
			X = result[0][1:]
			data_S = data_S[1:]
			S = result[1][1:]
			data_E = data_E[1:]
			E = result[2][1:]

			# we may not normalize nor standardize here. We must normalize only after the concatenation.
			
			nOBS = nOBS + data_X.tolist() + data_S.tolist() + data_E.tolist()
			nPRED = nPRED + X.tolist() + S.tolist() + E.tolist()

		return mySafeMSE(nOBS,nPRED)


	def R2(self, beta, *args):
		"""dataset[0]: biomass, 
		dataset[1]: substrate, 
		dataset[2]: product, 
		dataset[3]: time"""

		def removenans(data, modelled):
			"""remove NaN values from measurements and corresponding modeled values"""
			nans = np.isnan(data)
			return np.compress(~nans,data), np.compress(~nans,modelled)

		# args = map(list, args)
		args = list(map(list, args))

		# good_data = []
		nOBS = []
		nPRED = []

		for dataset in args[0]:
			# print("Parameters: {0}".format(beta))
			# print "Time",dataset[3]

			data_X = dataset[0]
			data_S = dataset[1]
			data_E = dataset[2]
			data_t = dataset[3]
			result = self.solution(beta,data_t, start_X=data_X[0], start_S=data_S[0], start_E=data_E[0])
			
			# this is the right place to skip first values
			data_X = data_X[1:]
			X = result[0][1:]
			data_S = data_S[1:]
			S = result[1][1:]
			data_E = data_E[1:]
			E = result[2][1:]

			# we may not normalize nor standardize here. We must normalize only after the concatenation.
			
			nOBS = nOBS + data_X.tolist() + data_S.tolist() + data_E.tolist()
			nPRED = nPRED + X.tolist() + S.tolist() + E.tolist()

		# This is the right place for normalization
		# nOBS, nPRED = zero_mean_variance(nOBS, nPRED)
		nOBS, nPRED = feature_scaling(nOBS, nPRED)
		# results_table = DataFrame(columns=['nOBS','nPRED'], data={'nOBS': nOBS, 'nPRED': nPRED})
		# results_table.to_html("obspredtable.html")

		return mySafeR2(nOBS, nPRED)



	def simulation(self,dataset, **kwargs):
		if 'parameters' in kwargs:
			modeled_values = self.solution(kwargs['parameters'],dataset[3], start_X=dataset[0][0], start_S = dataset[1][0], start_E=dataset[2][0])
		elif len(self.parameters)>2:
			modeled_values = self.solution(self.parameters,dataset[3], start_X=dataset[0][0], start_S = dataset[1][0], start_E=dataset[2][0])
		else:
			return False

		if 'expno' in kwargs:
			expno = kwargs['expno']
		else:
			expno = "Unknown"

		"""dataset[0]: biomass, 
		dataset[1]: substrate, 
		dataset[2]: product, 
		dataset[3]: time"""
		modeled_values = np.transpose(modeled_values)
		results_table = DataFrame(modeled_values)
		results_table.columns = ['X model','S model','P model']
		results_table['X'] = dataset[0]
		results_table['S'] = dataset[1]
		results_table['P'] = dataset[2]
		results_table['h'] = dataset[3]
		##round values
		results_table['X model'] = results_table['X model'].round(4)
		results_table['S model'] = results_table['S model'].round(4)
		results_table['P model'] = results_table['P model'].round(4)
		results_table['expno'] = expno

		results_table = DataFrame(results_table, columns=['h','X','X model','S', 'S model', 'P', 'P model', 'expno'])
		return results_table


	def statistics_could_be_wrong(self,beta,results_table1, **kwargs):
		corX = round(mycorrelation(results_table1['X'],results_table1['X model']),4)
		corS = round(mycorrelation(results_table1['S'],results_table1['S model']),4)
		corP = round(mycorrelation(results_table1['P'],results_table1['P model']),4)
		MSE  = self.fit(beta, results_table1['X'], results_table1['S'], results_table1['P'], results_table1['h'])

		index = '{0}_{1}'.format(self.name, kwargs['expno'])
		results_table = DataFrame({'R2 X':corX, 'R2 S':corS, 'R2 P':corP, 'MSE':MSE, 'expno':kwargs['expno'],'model':self.name}, index=[index])
		results_table = DataFrame(results_table, columns=['model','expno','MSE','R2 X', 'R2 S', 'R2 P'])

		if os.path.isfile('statistics.pickle'):
			all_stats = pd.read_pickle('statistics.pickle')
			# deletethis = DataFrame({'R2 X':0, 'R2 S':0, 'R2 P':0, 'MSE':0, 'expno':0,'model':0}, index=[index])
			all_stats = all_stats.append(DataFrame({'R2 X':0, 'R2 S':0, 'R2 P':0, 'MSE':0, 'expno':0,'model':0}, index=[index]))

			all_stats.drop(index, axis=0, inplace=True)

			all_stats = all_stats.append(results_table)
			all_stats.to_pickle('statistics.pickle')
			all_stats.to_html('statistics.html')
			return all_stats
		else:
			print("File for statistics does not exist...creating a new one.")
			results_table.to_pickle('statistics.pickle')
			results_table.to_html('statistics.html')

		return results_table








class dynamicmodel4(dynamicmodel):
	def fit(self, beta, data_t, data_X, data_S, data_P1, data_P2):
		"""Objective function for this model"""
		def removenans(data, modelled):
			"""remove NaN values from measurements and corresponding modeled values"""
			nans = np.isnan(data)
			return np.compress(~nans,data), np.compress(~nans,modelled)
		# start_X, miu_max, Yxs, Yps, mS, Ks, Kis, Kip = beta
		
		result = self.solution(beta,data_t, start_X=data_X[0], start_S=data_S[0], start_P1=data_P1[0], start_P2=data_P2[0])
		# remove NaN values
		data_X, X = removenans(data_X, result[0])
		data_S, S = removenans(data_S, result[1])
		data_P1,P1 = removenans(data_P1, result[2])
		data_P2,P2 = removenans(data_P2, result[3])

		residX  = np.sum( np.power(resc(X, data_X)-resc(data_X,data_X), 2) )
		residS  = np.sum( np.power(resc(S, data_S)-resc(data_S,data_S), 2) )
		residP1 = np.sum( np.power(resc(P1, data_P1)-resc(data_P1,data_P11), 2) )
		residP2 = np.sum( np.power(resc(P2, data_P2)-resc(data_P2,data_P22), 2) )
		
		resid = residX + residS + residP1 + residP2

		return 1./(len(data_X) + len(data_S) + len(data_P1) + len(data_P2) ) * resid

	def multi_fit(self, beta, *args):
		"""dataset[0]: time, 
		dataset[1]: biomass, 
		dataset[2]: substrate, 
		dataset[3]: product1
		dataset[4]: product2"""
		
		fitness_all = [] # local max values

		# print(len(args))
		# for count, thing in enumerate(args):
		# 	print '{0}. {1}'.format(count, thing)

		for dataset in args:
			# print("Parameters: {0}".format(beta))
			# print "Time",dataset[3]
			fitness = self.fit(beta, dataset[0], dataset[1], dataset[2], dataset[3], dataset[4])
			fitness_all.append(fitness)
		return np.sum(fitness_all)

	def simulation(self,dataset, **kwargs):
		if 'parameters' in kwargs:
			modeled_values = self.solution(kwargs['parameters'],dataset[0], start_X=dataset[1][0], start_S = dataset[2][0], start_P1=dataset[3][0], start_P2=dataset[4][0])
		elif len(self.parameters)>2:
			modeled_values = self.solution(self.parameters,dataset[0], start_X=dataset[1][0], start_S = dataset[2][0], start_P1=dataset[3][0], start_P2=dataset[4][0])
		else:
			return False

		"""dataset[0]: time, 
		dataset[1]: biomass, 
		dataset[2]: substrate, 
		dataset[3]: product1
		dataset[4]: product2"""
		modeled_values = np.transpose(modeled_values)
		results_table = DataFrame(modeled_values)
		results_table.columns = ['X model','S model','P1 model','P2 model']
		
		results_table['h'] = dataset[0]
		results_table['X'] = dataset[1]
		results_table['S'] = dataset[2]
		results_table['P1'] = dataset[3]
		results_table['P2'] = dataset[4]
		
		##round values
		results_table['X model'] = results_table['X model'].round(4)
		results_table['S model'] = results_table['S model'].round(4)
		results_table['P1 model'] = results_table['P1 model'].round(4)
		results_table['P2 model'] = results_table['P2 model'].round(4)

		results_table = DataFrame(results_table, columns=['h','X','X model','S', 'S model', 'P1', 'P1 model', 'P2', 'P2 model'])
		return results_table


