#!/usr/bin/python
# -*- coding: utf-8 -*-

# ZafarLP
def model_options(model_name,exptitle):
	if model_name == 'zafarLP':
		"""Andrews"""
		from ethanol_models import zafarLP
		path = "rezultati_zafarLP/"
		model = zafarLP("zafarLP", exptitle=exptitle, path=path, save=True)
		#          miu_max, 	Yxs, 	   	mS, 	Ks,          Kis,      Kip,   a,       b
		bounds = [(0.01, 0.75), (0, 0.9), (0, 0.5), (0.1, 50), (0, 600), (0, 30), (0, 5), (0, 5)]

	elif model_name == 'model2':
		from ethanol_models import model2
		path = "rezultati_model2/"
		model = model2("model2", exptitle=exptitle, path=path, save=True)
		#         miu_max,        Yxs,     mS,      Ks,       Kis,      Kip,     a,      b
		bounds = [(0.1, 0.75), (0, 0.9), (0, 0.5), (0.1, 50), (0, 600), (0, 80), (0, 5), (0, 5)]

	elif model_name == 'haldaneAndrewsLP':
		"""Haldane-Andrews"""
		from ethanol_models import haldaneAndrewsLP
		path = "rezultati_haldaneAndrewsLP/"
		model = haldaneAndrewsLP("haldaneAndrewsLP", exptitle=exptitle, path=path, save=True)
		#         miu_max,        Yxs,     mS,      Ks,       Kis,      Kip,     a,      b
		bounds = [(0.1, 0.75), (0, 0.9), (0, 0.5), (0.1, 50), (0, 600), (0, 80), (0, 5), (0, 5)]

	elif model_name == 'AibaEdwardsLP':
		"""Aiba-Edwards"""
		from ethanol_models import AibaEdwardsLP
		path = "rezultati_AibaEdwardsLP/"
		model = AibaEdwardsLP("AibaEdwardsLP", exptitle=exptitle, path=path, save=True)
		#         miu_max,        Yxs,     mS,      Ks,       Kis,      Kip,     a,      b
		bounds = [(0.1, 0.75), (0, 0.9), (0, 0.5), (0.1, 50), (0, 600), (0, 80), (0, 5), (0, 5)]


	return path, model, bounds
